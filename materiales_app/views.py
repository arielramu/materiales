from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages



#----------------------------
def logIn(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(request, username=username, password=password)
		if user is not None:
			login(request, user)
			return redirect('list')
		else:
			messages.error(request, f'Email o Password incorrectos. Por favor intente de nuevo.')

	context = {
		'title': 'login',
	}
	return render(request, 'materiales_app/login.html', context)

#----------------------------
@login_required(login_url='login')
def logOut(request):
	logout(request)
	context = {
		'title': 'logout',
	}
	return render(request, 'materiales_app/logout.html', context)


#----------------------------
@login_required(login_url='login')
def list(request):

	context = {
		'title': 'Listado',
	}
	return render(request, 'materiales_app/list.html', context)

